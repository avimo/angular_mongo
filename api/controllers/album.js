'use strict'

var fs 					= require('fs');
var path 				= require('path')
var mongoosePaginate 	= require('mongoose-pagination')

var Artist 				= require('../models/artist')
var Album 				= require('../models/album')
var Song 				= require('../models/song')


function getAlbum(req,res){
	
	//res.status(200).send({message:'albumes'});
	var albumId 		= req.params.id;
	Album.findById(albumId).populate({path:'artist'}).exec((err,album)=>{
		if(err){
			res.status(500).send({message:'Error al buscar el album'});
		}else{
			if(!album){
				res.status(404).send({message:'Album no encontrado'});
			}else{
				res.status(200).send({ album:album })
			}
		}
	});
}


function saveAlbum(req,res){
	var album  	= new Album;
	var params 	= req.body;

	album.title 	 	= params.title;
	album.description 	= params.description;
	album.year 			= params.year;
	album.image   		= 'null';
	album.artist   		= params.artist;
	
	album.save((err,albumStored) => {
		if(err){
			res.status(500).send({message:'Error al almacenar el album',err:err});
		}else{
			if(!albumStored){
				res.status(404).send({message:'No se registró el album'});
			}else{
				res.status(200).send({ artist:albumStored })
			}
		}
	});
			

};	


function getAlbums(req,res){
	var artistId 		= req.params.artist

	if(!artistId){
		var find= Album.find().sort('title');
	}else{
		var find= Album.find({artist:artistId}).sort('year');
	}
	find.populate({path:'artist'}).exec((err,albums)=>{
		if(err){
			res.status(500).send({message:'Error en la consulta de albums',err:err});
		}else{
			if (!albums) {
				res.status(404).send({message:'No se encontraron albums'});
			}else{
				res.status(200).send({ artist:albums })
			}
		}
	})
};	


function updateAlbum(req,res){
	var albumId 		= req.params.id;
	var update 			= req.body;
	
	Album.findByIdAndUpdate(albumId,update,(err,albumUpdated) => {
		if (err) {
			res.status(500).send({message:'Error en la actualización de artista'});
		}else{
			if(!albumUpdated){
				res.status(404).send({message:'No se encontró al artista'});
			}else{
				Album.findById(albumId,(err,album)=>{
					res.status(200).send({ album:album })
				})
			}
		}
	})
};	

function deleteAlbum(req,res){
	var albumId 		= req.params.id;
	
	
	Album.findByIdAndRemove(albumId,(err,albumRemoved) => {
		if (err) {
			res.status(500).send({message:'Error en la actualización de artista'});
		}else{
			if(!albumRemoved){
				res.status(404).send({message:'No se encontró al artista'});
			}else{
				
				//res.status(200).send({ artista:artistRemoved });

				Album.find({artist:albumRemoved._id}).remove((err,albumRemoved)=>{
					
					if(err){
							res.status(500).send({message:"Error al eliminar el album"})
					}else{
						if(!albumRemoved){
							res.status(404).send({message:"El album no pudo ser eliminado"})
						}else{
							Song.find({artist:albumRemoved._id}).remove((err,songRemoved)=>{
								if(err){
										res.status(500).send({message:"Error al eliminar canciones"})
								}else{
									if(!songRemoved){
										res.status(404).send({message:"No se encontraron canciones"})
									}else{
										res.status(200).send({message:"Se eliminó el album",album:albumRemoved})
									}
								}
							})
						}
					}
				})
				
			}
		}
	})

};	


function uploadImage(req,res){
	var albumId  	= req.params.id;
	var file_name 	= 'No subido...';
	if(req.files){
		var file_path = req.files.image.path;
		console.log(file_path)
		
		var file_split = file_path.split("\/")
		var file_name  = file_split[file_split.length-1];
		
		var ext_split  = file_path.split("\.")
		var file_ext   = ext_split[1]


		if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif' ){
			Album.findByIdAndUpdate(albumId,{image:file_name},(err,albumUpdated)=>{
				if (err) {
					res.status(500).send({message:'Error al actualizar el artista'})
				}else{
					if(!albumUpdated){
						res.status(404).send({message:'No se pudo actualizar artista'})	
					}else{
						Album.findOne({_id:albumId},(err,albumUpdated)=>{
							res.status(200).send({album:albumUpdated,estatus:'Actualizado Imagen',file_name:file_name});	
						})
						
					}	
				}
			})
		}else{
			res.status(404).send({message:"Extensión no válida"});
		}

	}else{
		res.status(200).send({message:"No se ha subido ninguna imagen"});
	}


}

function getImageFile(req,res){
	
	var imageFile = req.params.imageFile;
	var path_file = './uploads/albums/'+imageFile

	fs.exists(path_file,function(exists){
		if (exists) {
			res.sendFile(path.resolve(path_file));
		}else{
			res.status(404).send({message:"Imagen no encontrada"});
		}

	})
}


module.exports = {
	getAlbum
	,saveAlbum
	,getAlbums
	,updateAlbum
	,deleteAlbum
	,uploadImage
	,getImageFile
};