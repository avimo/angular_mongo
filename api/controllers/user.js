'use strict'

var fs 			= require('fs');
var path 		= require('path')
var bcrypt 		= require('bcrypt-nodejs');
var User  		= require('../models/user');
var jwt 		= require('../services/jwt');

function pruebas(req, res){
	res.status(200).send({
		message: 'Controlador user'
	})
};

function saveUserValidation(new_user){
	var errors=[]
	var cat_errors=[
		'Este campo es obligatorio'
		,'Campo inválido'
		,'Las contraseñas no coinciden'
	]
	if (new_user.name==null) {
		errors.push({'error':cat_errors[0],'field':'Nombre','code':200})
	}
	if (new_user.surname==null) {
		errors.push({'error':cat_errors[0],'field':'Apellido','code':200})
	}
	if (new_user.email==null) {
		errors.push({'error':cat_errors[0],'field':'Correo','code':200})
	}
	if (new_user.password==null) {
		errors.push({'error':cat_errors[0],'field':'Password','code':200})
	}
	if (new_user.passwordconf==null) {
		errors.push({'error':cat_errors[0],'field':'Confirmación de Password','code':200})
	}
	if (new_user.password!=new_user.passwordconf) {
		errors.push({'error':cat_errors[2],'field':'Password','code':200})
	}

	return errors;


}

function saveUser(req,res){
	var user  	= new User;
	var params 	= req.body;

	console.log(params)
	var errors=saveUserValidation(params)
	if (errors.length > 0) {
		res.status(200).send({ errors:errors })
	}else{
		
		User.findOne({ email : params.email }, (err,user) => {
			if(err){
				res.status(500).send({message:"Error en la petición"});
			}else{
				if(!user){
					var user  	= new User;
					user.name 	 	= params.name;
					user.surname 	= params.surname;
					user.email 	 	= params.email;
					user.role 	 	= params.role;
					user.image   	= 'null';	
					bcrypt.hash(params.password,null,null,function(err,hash){
						user.password=hash;
						user.save((err,userStored) => {
							if(err){
								res.status(500).send({message:'Error al almacenar el usuario'});
							}else{
								if(!userStored){
									res.status(404).send({message:'No se registró el usuario'});
								}else{
									res.status(200).send({ user:userStored })
									console.log({user:userStored,message:"Se creó usuario"})
								}
							}
						});
					});

				}else{
					res.status(404).send({message:"El usuario ya se registró anteriormente"});
				}
			}

		})
		
	}

}

function updateUser(req,res){
	var userId  	= req.params.id;
	var update 		= req.body;

	if (userId != req.user.sub) {
		return res.status(500).send({message:"No tiene permisos para realizar este cambio"})
	}

	User.findByIdAndUpdate(userId,update,(err,userUpdated) => {
		if (err) {
			res.status(500).send({message:'Error al actualizar el usuario'})
		}else{
			if(!userUpdated){
				res.status(404).send({message:'No se pudo actualizar usuario'})	
			}else{
				res.status(200).send({user:update,estatus:'Actualizado'});
				console.log({user:update,estatus:'Actualizado'})
			}

		}

	});

	

	
};	
	

function loginUser(req,res){

	var params 		= req.body;

	var email 		= params.email;
	var password 	= params.password;

	console.log(params)

	User.findOne({ email : email }, (err,user) => {
		if(err){
			res.status(500).send({message:"Error en la petición"});
		}else{
			if(!user){
				res.status(404).send({message:"El usuario no existe"});
			}else{
				bcrypt.compare(password,user.password,function(err,check){
					if( check ){
						// Devolver datos de usuario logueado
						if(params.gethash){
							// Devolver token jwt
							res.status(200).send({
								token: jwt.createToken(user)
							});

						}else{
							res.status(200).send({user:user});
						}
					}else{
						res.status(200).send({message:"El usuario no pudo loguearse"});
					}
				});
			}
		}

	})
}


function uploadImage(req,res){
	var userId = req.params.id;
	var filename = 'no subido...';
	console.log(req.files)
	if(req.files){
		var file_path = req.files.image.path;
		console.log(file_path)
		
		var file_split = file_path.split("\/")
		var file_name  = file_split[file_split.length-1];
		
		var ext_split  = file_path.split("\.")
		var file_ext   = ext_split[1]


		console.log(file_split)
		console.log(file_name)
		

		if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif' ){
			User.findByIdAndUpdate(userId,{image:file_name},(err,userUpdated)=>{
				if (err) {
					res.status(500).send({message:'Error al actualizar el usuario'})
				}else{
					if(!userUpdated){
						res.status(404).send({message:'No se pudo actualizar usuario'})	
					}else{
						User.findOne({_id:userId},(err,userUpdated)=>{
							res.status(200).send({user:userUpdated,estatus:'Actualizado Imagen',image:file_name});	
						})
						
					}	
				}
			})
		}else{
			res.status(404).send({message:"Extensión no válida"});
		}

	}else{
		res.status(200).send({message:"No se ha subido ninguna imagen"});
	}

}

function getImageFile(req,res){
	
	var imageFile = req.params.imageFile;
	var path_file = './uploads/users/'+imageFile

	fs.exists(path_file,function(exists){
		if (exists) {
			res.sendFile(path.resolve(path_file));
		}else{
			res.status(404).send({message:"Imagen no encontrada"});
		}

	})
}

module.exports = {
	pruebas,
	saveUser,
	loginUser,
	updateUser,
	uploadImage,
	getImageFile
};