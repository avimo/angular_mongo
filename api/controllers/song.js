'use strict'

var fs 					= require('fs');
var path 				= require('path')
var mongoosePaginate 	= require('mongoose-pagination')

var Artist 				= require('../models/artist')
var Album 				= require('../models/album')
var Song 				= require('../models/song')


function getSong(req,res){
	var songId = req.params.id

	Song.findById(songId).populate({path:'album'}).exec((err,song)=>{
		if(err){
			res.status(500).send({ message:"Error de servidor" })
		}else{
			if (!song) {
				res.status(404).send({ message:"No se encontró la canción" })
			}else{
				res.status(200).send({ song:song })
			}
		}
	})

}
function getSongs(req,res){
	var albumId = req.params.album

	if(!albumId){
		var find= Song.find({}).sort('number')
	}else{
		var find= Song.find({album:albumId}).sort('number')
	}
	find.populate({
		path:'album'
		,populate:{
			path:'artist'
			,model:'Artist'
		}
	}).exec(function(err,songs){
		if (err) {
			res.status(500).send({ message:"Error de servidor" });
		}else{
			if (!songs) {
				res.status(404).send({ message:"No se encontraron canciones" })
			}else{
				res.status(200).send({ songs:songs })
			}
		}
	})

}

function updateSong(req,res){
	var songId = req.params.id
	var update = req.body

	Song.findByIdAndUpdate(songId,update,(err,songUpdated)=>{
		if (err) {
			res.status(500).send({ message:"Error de servidor" });
		}else{
			if (!songUpdated) {
				res.status(404).send({ message:"No se encontraron canciones" })
			}else{
				Song.findById(songId,(err,song)=>{
					res.status(200).send({ song:song })
				})
			}
		}
	})

}

function deleteSong(req,res){
	var songId = req.params.id

	Song.findByIdAndRemove(songId,(err,songRemoved)=>{
		if(err){
			res.status(500).send({ message:"Error de servidor" })
		}else{
			if (!songRemoved) {
				res.status(404).send({ message:"No se encontró la canción" })
			}else{
				res.status(200).send({ message:"Se borró la canción ",song:songRemoved })
			}
		}
	})
}


function saveSong(req,res){
	var song= new Song;
	var params= req.body

	song.number		= params.number;
	song.name 		= params.name;
	song.duration 	= params.duration;
	song.file 		= null;
	song.album 		= params.album;

	song.save((err,songStored)=>{
		if(err){
			res.status(500).send({ message:"Error de servidor" })
		}else{
			if (!songStored) {
				res.status(404).send({ message:"No se almacennó la canción" })
			}else{
				res.status(200).send({ message:"Se almacenó la canción",song:songStored })
			}
		}
	})
}

function uploadFile(req,res){
	var songId  	= req.params.id;
	var file_name 	= 'No subido...';
	console.log(req.files)
	if(req.files){
		var file_path = req.files.file.path;
		
		var file_split = file_path.split("\/")
		var file_name  = file_split[file_split.length-1];
		
		var ext_split  = file_path.split("\.")
		var file_ext   = ext_split[1]


		if(file_ext == 'mp3' ){
			Song.findByIdAndUpdate(songId,{image:file_name},(err,songUpdated)=>{
				if (err) {
					res.status(500).send({message:'Error al actualizar el artista'})
				}else{
					if(!songUpdated){
						res.status(404).send({message:'No se pudo actualizar artista'})	
					}else{
						Song.findOne({_id:songId},(err,songUpdated)=>{
							res.status(200).send({song:songUpdated,estatus:'Actualizado Imagen',file_name:file_name});	
						})
						
					}	
				}
			})
		}else{
			res.status(404).send({message:"Extensión no válida"});
		}

	}else{
		res.status(200).send({message:"No se ha subido ninguna imagen"});
	}

}
function getFile(req,res){
	
	var songFile = req.params.songFile;
	var path_file = './uploads/songs/'+songFile

	fs.exists(path_file,function(exists){
		if (exists) {
			res.sendFile(path.resolve(path_file));
		}else{
			res.status(404).send({message:"Canción no encontrada"});
		}

	})
}

module.exports = {
	getSong
	,saveSong
	,getSongs
	,updateSong
	,deleteSong
	,uploadFile
	,getFile

};