'use strict'

var fs 					= require('fs');
var path 				= require('path')
var mongoosePaginate 	= require('mongoose-pagination')

var Artist 				= require('../models/artist')
var Album 				= require('../models/album')
var Song 				= require('../models/song')


function getArtist(req,res){
	//res.status(200).send({message:'Método getArtist'})
	var artistId 		= req.params.id;
	Artist.findById(artistId,(err,artist)=>{
		if(err){
			console.log(req.params.id)
			console.log(err)
			console.log('Error al buscar el artista')
			res.status(500).send({message:'Error al buscar el artista'});
		}else{
			if(!artist){
				res.status(404).send({message:'Artista no encontrado'});
			}else{
				console.log(artist)
				res.status(200).send({ artist:artist })

			}
		}
	});
}


function saveArtist(req,res){
	var artist  	= new Artist;

	var params 	= req.body;

	artist.name 	 	= params.name;
	artist.description 	= params.description;
	artist.image   	= 'null';
	console.log('recibido:')	
	console.log(artist.name)
	console.log(artist.description)
	artist.save((err,artistStored) => {
		if(err){
			res.status(500).send({message:'Error al almacenar el artista'});
		}else{
			if(!artistStored){
				res.status(404).send({message:'No se registró el artista'});
			}else{
				res.status(200).send({ artist:artistStored })
				console.log(artistStored)
			}
		}
	});
			

};	


function getArtists(req,res){
	var page 			= 1;
	if (req.params.page){
		page 			= req.params.page;
	}
	
	var itemsPerPage 	= 3;
	//var artist  		= new Artist;
	Artist.find().sort('_id').paginate(page,itemsPerPage,function(err,artists,total){
		if(err){
			console.log('Error en el listado de artistas')
			res.status(500).send({message:'Error en el listado de artistas'});
		}else{
			if(!artists){
				res.status(404).send({message:'No hay artistas almacenados'});
			}else{
				res.status(200).send({total_items:total,artists:artists});
			}
		}
	})

			

};	

function updateArtist(req,res){
	var artistId 		= req.params.id;
	var update 			= req.body;
	
	Artist.findByIdAndUpdate(artistId,update,(err,artistUpdated) => {
		if (err) {
			res.status(500).send({message:'Error en la actualización de artista'});
		}else{
			if(!artistUpdated){
				res.status(404).send({message:'No se encontró al artista'});
			}else{
				Artist.findById(artistId,(err,artist)=>{
					res.status(200).send({ artist:artist })
				})
			}
		}
	})

};	

function deleteArtist(req,res){
	var artistId 		= req.params.id;
	
	
	Artist.findByIdAndRemove(artistId,(err,artistRemoved) => {
		if (err) {
			res.status(500).send({message:'Error en la actualización de artista'});
		}else{
			if(!artistRemoved){
				res.status(404).send({message:'No se encontró al artista'});
			}else{
				
				//res.status(200).send({ artista:artistRemoved });

				Album.find({artist:artistRemoved._id}).remove((err,albumRemoved)=>{
					
					if(err){
							res.status(500).send({message:"Error al eliminar el album"})
					}else{
						if(!albumRemoved){
							res.status(404).send({message:"El album no pudo ser eliminado"})
						}else{
							Song.find({artist:albumRemoved._id}).remove((err,songRemoved)=>{
								if(err){
										res.status(500).send({message:"Error al eliminar canciones"})
								}else{
									if(!songRemoved){
										res.status(404).send({message:"No se encontraron canciones"})
									}else{
										res.status(200).send({message:"Se eliminó el artista",artist:artistRemoved})
									}
								}
							})
						}
					}
				})
				
			}
		}
	})

};	


function uploadImage(req,res){
	var artistId  	= req.params.id;
	var file_name 	= 'No subido...';
	console.log(req.files)
	if(req.files){
		var file_path = req.files.image.path;
		console.log(file_path)
		
		var file_split = file_path.split("\/")
		var file_name  = file_split[file_split.length-1];
		
		var ext_split  = file_path.split("\.")
		var file_ext   = ext_split[1]


		console.log(file_split)
		console.log(file_name)
		

		if(file_ext == 'png' || file_ext == 'jpg' || file_ext == 'gif' ){
			Artist.findByIdAndUpdate(artistId,{image:file_name},(err,artistUpdated)=>{
				if (err) {
					res.status(500).send({message:'Error al actualizar el artista'})
				}else{
					if(!artistUpdated){
						res.status(404).send({message:'No se pudo actualizar artista'})	
					}else{
						Artist.findOne({_id:artistId},(err,artistUpdated)=>{
							res.status(200).send({artist:artistUpdated,estatus:'Actualizado Imagen',file_name:file_name});	
						})
						
					}	
				}
			})
		}else{
			res.status(404).send({message:"Extensión no válida"});
		}

	}else{
		res.status(200).send({message:"No se ha subido ninguna imagen"});
	}

}
function getImageFile(req,res){
	
	var imageFile = req.params.imageFile;
	var path_file = './uploads/artists/'+imageFile

	fs.exists(path_file,function(exists){
		if (exists) {
			res.sendFile(path.resolve(path_file));
		}else{
			res.status(404).send({message:"Imagen no encontrada"});
		}

	})
}


module.exports = {
	getArtist,
	saveArtist,
	getArtists,
	updateArtist,
	deleteArtist,
	uploadImage,
	getImageFile
};