import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { routing, appRoutingProviders} from './app.routing';

import { AppComponent } from './app.component';
import { ForbiddenComponent } from './components/forbidden.component';
import { HomeComponent } from './components/home.component';
import { UserEditComponent } from './components/user-edit.component';
import { AppSidebarComponent } from './components/app-sidebar.component';
import { ArtistListComponent } from './components/artist-list.component';
import { ArtistAddComponent } from './components/artist-add.component';
import { ArtistEditComponent } from './components/artist-edit.component';
import { ArtistDetailComponent } from './components/artist-detail.component';


import * as $ from 'jquery'

@NgModule({
  declarations: [
    AppComponent
    ,ForbiddenComponent
    ,HomeComponent
    ,UserEditComponent
    ,AppSidebarComponent
    ,ArtistListComponent
    ,ArtistAddComponent
    ,ArtistEditComponent
    ,ArtistDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
