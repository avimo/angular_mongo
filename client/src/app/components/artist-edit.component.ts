import {Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute,Params}  from '@angular/router'

import {GLOBAL} from '../services/global'
import {UserService} from '../services/user.service'
import {UploadService} from '../services/upload.service'
import {ArtistService} from '../services/artist.service'
import {Artist} from '../models/artist'

@Component({
	selector:'artist-edit',
	templateUrl: '../views/artist-add.html',
	providers: [UserService,ArtistService,UploadService]
}) 

export class ArtistEditComponent implements OnInit{
	public titulo : string;
	public artist : Artist;
	public identity;
	public token;
	public url:string;
	public alertMessage;
	public alertStyle;
	public is_edit;
	public sub_txt:string;
	constructor (
			private _route:ActivatedRoute,
			private _router: Router,
			private _userService:UserService,
			private _artistService:ArtistService,
		 	private _uploadService:UploadService

		){
		this.titulo = 'Editar artista';
		this.identity=this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		this.artist=new Artist('','','');
		this.is_edit=true;
		this.sub_txt='Guardar';

	}

	ngOnInit(){
		console.log('artist-edit.component.ts cargado');
		this.getArtist()
	}

	getArtist(){
		this._route.params.forEach((params:Params)=>{
			let id = params['id'];
			this._artistService.getArtist(this.token,id).subscribe(
					response=>{
						this.artist= response.artist;
						if (!response.artist) {
							console.log("no hay artista")		
							this._router.navigate(['/'])	
							
						}else{
							this.artist=response.artist;
							console.log(this.artist)
						}
					},
					error=>{
						var errorMessage = <any>error;
		  				if(errorMessage!=null){
		  					var body = JSON.parse(error._body)
		  					//this.errorMessage=body.message;
		  					console.log(error);
		  					this._router.navigate(['/'])
		  				}
					}

				)
		})
	}

	onSubmit(){
		this._route.params.forEach((params:Params)=>{
			let id = params['id'];
			console.log(this.artist)
			this._artistService.editArtist(this.token,id,this.artist).subscribe(
				response=>{
					this.artist=response.artist;
					if(!response.artist){
						this.alertMessage="Error en el servidor";
						this.alertStyle="danger";
					}else{
						console.log("success..")
						this.alertMessage="Artista editado correctamente";
						this.alertStyle="info";
						//this.artist=response.artist;
						//this._router.navigate(['/editar-artista'],response.artist._id);
						this._uploadService.makeFileRequest(this.url+'upload-image-artist/'+id, [], this.filesToUpload,this.token,'image')
							.then(
									(result)=> {
										this._router.navigate(['/artistas',1])
									} ,
									(error)=> {
										console.log(error)
									}
								);
					}
				},
				error=>{
					var errorMessage = <any>error;
	  				if(errorMessage!=null){
	  					var body = JSON.parse(error._body)
	  					//this.errorMessage=body.message;
	  					console.log(error);
	  				}
				}
			)

		})
	}

	public filesToUpload: Array<File>;
	fileChangeEvent(fileInput:any){
		this.filesToUpload= <Array<File>>fileInput.target.files;
	}

}
