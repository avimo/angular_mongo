import { Component, OnInit } from '@angular/core';

import { GLOBAL} from '../services/global';
import { UserService } from '../services/user.service';
import { User } from '../models/user';

@Component({
	selector: 'user-edit',
	templateUrl: '../views/user-edit.html',
	providers: [UserService],


})


export class UserEditComponent implements OnInit{
	public titleEdit: string;
	public user:User;
	public identity;
	public token;
	public alertMessage;
	public alertStyle;
	
	public url:string;

	constructor(
				private _userService : UserService
		){
		
		this.titleEdit='Actualizar mis datos';
		
		
		//local storage
		this.identity= this._userService.getIdentity();
  		this.token= this._userService.getToken();
  		this.user= this.identity;
  		this.url = GLOBAL.url;
	}

	public ngOnInit(){ 	
		console.log("user-edit.component.ts cargado")
	}
	public onSubmit(){
		this._userService.updateUser(this.user).subscribe(

			response=>{  
	          	if (response.errors || !response.user){
		            
		            this.alertStyle="danger";
		            this.alertMessage=""
		            for (var i = 0 ; i < response.errors.length ; i++) {
		              response.errors[i]
		              this.alertMessage+="\n"+response.errors[i].field+" : \n"+response.errors[i].error
		            }
		          }else{
		          	this.user 	 		= response.user;
		            localStorage.setItem('identity',JSON.stringify(this.user));
		            document.getElementById('user-name-bnanner').innerHTML=this.user.name;

		            if (!this.filesToUpload) {
		            	// code...
		            }else{
		            	console.log(this.url+'upload-image-user/'+this.user._id);
		            	this.makeFileRequest(this.url+'upload-image-user/'+this.user._id,[],this.filesToUpload).then(
		            			(result:any) => {
		            				//console.log(this.url+'upload-image-user/'+this.user._id);
		            				console.log(result)
		            				console.log(this.user);

		            				this.user.image = result.image;
		            				localStorage.setItem('identity',JSON.stringify(this.user));
		            				let image_path=this.url+'get-image-user/'+this.user.image;
		            				document.getElementById('user-image-icon').setAttribute('src',image_path);
		            				//console.log(this.user);

									/* $.gritter.add({
									title:"This is a notice without an image!",
									text:"This will fade out after a certain amount of time."})	 	*/			

									
		            			} 
		            		);

		            }
		            
		            this.alertMessage="El usuario se actualizó de forma exitosa: "+this.user.email;
		            this.alertStyle 	="info";
		          }
		        }
		        ,error=>{
		          var alertMessage = <any>error;
		          if(alertMessage!=null){
		            var body = JSON.parse(error._body)
		            this.alertMessage=body.message;
		            console.log(error);
		          }
		        }
			)
	}

	public filesToUpload: Array<File>;

	fileChangeEvent(fileInput:any){
		this.filesToUpload = <Array<File>>fileInput.target.files;
		//console.log(this.filesToUpload)
	}

	makeFileRequest(url:string,params:Array<string>,files:Array<File>){
		var token = this.token;

		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();

			for( var i=0; i < files.length; ++i) {
				//console.log('append')
				//console.log(files[i])
				formData.append('image',files[i],files[i].name)
			}
			//console.log("Form data")
			//console.log(formData)

			xhr.onreadystatechange = function(){
				console.log(xhr.status);
				if (xhr.readyState==4) {
					if (xhr.status==200) {
						resolve(JSON.parse(xhr.response))
					}else{
						reject(xhr.response)
					}
					
				}
			}
			xhr.open('POST',url,true);
			xhr.setRequestHeader('Authorization',token);
			xhr.send(formData);
		});

	}
}