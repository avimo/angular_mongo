import { Component, OnInit } from '@angular/core';

import { UserService } from '../services/user.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: '../views/app-sidebar.html',
	providers: [UserService]

})


export class AppSidebarComponent implements OnInit{
	
	public identity;
	public token;

	constructor(
				private _userService : UserService
		){
		
		
		//local storage
		this.identity= this._userService.getIdentity();
  		this.token= this._userService.getToken();
	}

	public ngOnInit(){ 	
		
		console.log("sidebar cargado")
	}
	public onSubmit(){
	}
}