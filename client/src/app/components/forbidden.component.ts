import {Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute,Params}  from '@angular/router'

import {GLOBAL} from '../services/global'

@Component({
	selector:'artist-edit',
	templateUrl: '../views/error.html',
	providers: []
}) 

export class ForbiddenComponent implements OnInit{
	public error_code : string;
	public error_message : string;
	constructor (
			private _route:ActivatedRoute,
			private _router: Router,
		

		){
		this.error_code='403';
		this.error_message = 'Acceso denegado';
		
	}

	ngOnInit(){
		console.log('forbidden.component.ts cargado');
	}

	

}
