import { Component,OnInit } 	from '@angular/core';
import { UserService } 			from '../services/user.service';
import { User } 				from '../models/user';


@Component({
  selector: 'user-login',
  templateUrl: '../views/user-login.html',
  providers: [UserService]
})

export class UserLoginComponent implements OnInit{
  public title = 'CoolPlayer';
  public user:User;
  public user_register:User;
  public identity;
  public token;
  public errorMessage;

  public alertRegister;
  public alertRegisterStyle;

  public alertErrorRegister;
  public alertErrorRegisterStyle;

  constructor(
  		private _userService:UserService
  	){
  	this.user= new User('','','','','','REG_USER','');
    this.user_register= new User('','','','','','REG_USER','');
  }

  public onSubmit(){

  	this._userService.signup(this.user).subscribe(

  			response=>{	
  				 				
  				let identity = response.user;
  				this.identity = identity;
  				
  				if (!response.user || !this.identity._id){
            this.errorMessage="El usuario no se logueó";
            this.alertErrorRegisterStyle="danger";
  				}else{
  					localStorage.setItem('identity',JSON.stringify(identity))
  					this._userService.signup(this.user,'true').subscribe(
  						response=>{
  							if (!response.token) { 
								alert("El token no se ha generado correctamente");
  							} else {
  								localStorage.setItem('token',response.token)
  								this.token=response.token;
                  this.user= new User('','','','','','REG_USER','');
  								
  							}
  						}
  					)
  				}
  			}
  			,error=>{
  				var errorMessage = <any>error;
  				if(errorMessage!=null){
  					var body = JSON.parse(error._body)
  					this.errorMessage=body.message;
  					console.log(error);
  				}
  			}
  		);
  }

  public onSubmitRegister(){
    this._userService.register(this.user_register).subscribe(

        response=>{  
          let user = response.user;
          this.user_register = user;
          
          if (response.errors && (!response.user || !this.user_register._id)){
           
            this.alertErrorRegisterStyle="danger";
            this.alertErrorRegister=""
            for (var i = 0 ; i < response.errors.length ; i++) {
              response.errors[i]
              this.alertErrorRegister+="\n"+response.errors[i].field+" : \n"+response.errors[i].error
            }
          }else{
            this.alertRegister="El usuario se registró de forma exitosa: "+user.email;
            this.alertRegisterStyle="info";
            this.user_register = new User('','','','','','REG_USER','');
          }
        }
        ,error=>{
          var errorMessage = <any>error;
          if(errorMessage!=null){
            var body = JSON.parse(error._body)
            this.errorMessage=body.message;
            console.log(error);
          }
        }
      );
  }

  public ngOnInit(){
  	this.identity= this._userService.getIdentity();
  	this.token= this._userService.getToken();


  }

  public logOut(){  	
  	localStorage.clear();

  	this.identity = null;
  	this.token = null;
    
  }


}
