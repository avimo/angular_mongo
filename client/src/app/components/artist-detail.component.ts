import {Component,OnInit} from '@angular/core';
import {Router,ActivatedRoute,Params}  from '@angular/router'

import {GLOBAL} from '../services/global'
import {UserService} from '../services/user.service'
import {ArtistService} from '../services/artist.service'
import {Artist} from '../models/artist'

@Component({
	selector:'artist-detail',
	templateUrl: '../views/artist-detail.html',
	providers: [UserService,ArtistService]
}) 


export class ArtistDetailComponent implements OnInit{
	public titulo : string;
	public artist : Artist;
	public identity;
	public token;
	public url:string;
	

	constructor (
			private _route:ActivatedRoute,
			private _router: Router,
			private _userService:UserService, 
			private _ArtistService:ArtistService

		){
		
		this.identity=this._userService.getIdentity();
		this.token = this._userService.getToken();
		this.url = GLOBAL.url;
		
	}

	ngOnInit(){
		console.log('artist-detail.component.ts cargado')
		this.getArtist()
	}
	getArtist(){
		this._route.params.forEach((params:Params)=>{
			let id=params['id']
			console.log(id)
			this._ArtistService.getArtist(this.token,id).subscribe(
					response=>{
						this.artist= response.artist;
						if (!response.artist) {
							console.log("no hay artistas")		
							//this._router.navigate(['/'])	
							
						}else{
							this.artist=response.artist;
							
							this.titulo=this.artist.name
							console.log(this.artist)
							

						}
					},
					error=>{
						var errorMessage = <any>error;
		  				if(errorMessage!=null){
		  					var body = JSON.parse(error._body)
		  					//this.errorMessage=body.message;
		  					console.log(error);
		  					//this._router.navigate(['/'])
		  				}
					}
				)
		})
	}
}
